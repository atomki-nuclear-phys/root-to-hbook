# Copyright (C) 2016-2022 Attila Krasznahorkay.
#
# Finds a ROOT instalation.
#
# It defines:
#   - ROOT_FOUND          Flag for whether ROOT is found
#   - ROOT_INCLUDE_DIRS   Path to the include directory
#   - ROOT_LIBRARIES      Libraries needed for the project
#   - ROOT_DEFINITIONS    Compiler definitions
#   - ROOT_CXX_FLAGS      Compiler flags to used by client packages
#   - ROOT_C_FLAGS        Compiler flags to used by client packages
#

# First try to find ROOT using the CMake code bundled with ROOT itself.
set( _pathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )
find_package( ROOT QUIET )

# If it was found like that, yay!
if( ROOT_FOUND )

   # Set/find some "compatibility variables".
   find_program( ROOT_CONFIG_EXECUTABLE "root-config"
      HINTS "${ROOT_BINARY_DIR}" )
   mark_as_advanced( ROOT_CONFIG_EXECUTABLE )

   # Make use of the imported libraries.
   if( "${ROOT_VERSION}" VERSION_LESS 6.10 )
      set( ROOT_LIBRARIES Core RIO Tree )
   else()
      set( ROOT_LIBRARIES ROOT::Core ROOT::RIO ROOT::Tree )
   endif()

else()

   # Look for root-config. Everything else is found using this script.
   find_program( ROOT_CONFIG_EXECUTABLE "root-config"
      HINTS "$ENV{ROOTSYS}/bin" )
   mark_as_advanced( ROOT_CONFIG_EXECUTABLE )

   # Get all the necessary variables:
   execute_process(
      COMMAND "${ROOT_CONFIG_EXECUTABLE}" "--prefix"
      OUTPUT_VARIABLE ROOTSYS
      OUTPUT_STRIP_TRAILING_WHITESPACE )

   execute_process(
      COMMAND "${ROOT_CONFIG_EXECUTABLE}" "--version"
      OUTPUT_VARIABLE ROOT_VERSION
      OUTPUT_STRIP_TRAILING_WHITESPACE )

   execute_process(
      COMMAND "${ROOT_CONFIG_EXECUTABLE}" "--incdir"
      OUTPUT_VARIABLE ROOT_INCLUDE_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   set( ROOT_INCLUDE_DIRS ${ROOT_INCLUDE_DIR} )

   execute_process(
      COMMAND "${ROOT_CONFIG_EXECUTABLE}" "--libdir"
      OUTPUT_VARIABLE ROOT_LIBRARY_DIR
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   set( ROOT_LIBRARY_DIRS ${ROOT_LIBRARY_DIR} )

   # Find all the needed libraries.
   set( _rootlibs Core RIO Tree )
   set( ROOT_LIBRARIES )
   foreach( _lib ${_rootlibs} )
      find_library( ROOT_${_lib}_LIBRARY ${_lib} HINTS ${ROOT_LIBRARY_DIR} )
      mark_as_advanced( ROOT_${_lib}_LIBRARY )
      list( APPEND ROOT_LIBRARIES ${ROOT_${_lib}_LIBRARY} )
   endforeach()

   # Construct the compiler flags.
   execute_process(
      COMMAND "${ROOT_CONFIG_EXECUTABLE}" --cflags
      OUTPUT_VARIABLE __cflags
      OUTPUT_STRIP_TRAILING_WHITESPACE )
   string( REGEX MATCHALL "-(D|U)[^ ]*" ROOT_DEFINITIONS "${__cflags}" )
   string( REGEX REPLACE "(^|[ ]*)-I[^ ]*" "" ROOT_CXX_FLAGS "${__cflags}" )
   string( REGEX REPLACE "(^|[ ]*)-I[^ ]*" "" ROOT_C_FLAGS "${__cflags}" )

endif()

# Handle the standard find_package arguments.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( ROOT
   FOUND_VAR ROOT_FOUND
   REQUIRED_VARS ROOT_CONFIG_EXECUTABLE ROOT_INCLUDE_DIRS ROOT_LIBRARIES
   VERSION_VAR ROOT_VERSION )

# Restore the module path.
set( CMAKE_MODULE_PATH ${_pathBackup} )
unset( _pathBackup )
