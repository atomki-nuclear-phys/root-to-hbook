// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_CERNLIB_DEFS_HPP
#define ROOT_TO_HBOOK_CERNLIB_DEFS_HPP

//
// Some CERNLIB definitions, stolen from ROOT's THbookFile.cxx.
//
#define PAWC_SIZE 4000000
#define pawc pawc_
#define quest quest_
#define hcbits hcbits_
#define hcbook hcbook_
#define rzcl rzcl_
int pawc[PAWC_SIZE] __attribute__((aligned(64)));
int quest[100] __attribute__((aligned(64)));
int hcbits[37] __attribute__((aligned(64)));
int hcbook[51] __attribute__((aligned(64)));
int rzcl[11] __attribute__((aligned(64)));

#endif  // ROOT_TO_HBOOK_CERNLIB_DEFS_HPP
