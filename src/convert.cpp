// Copyright (C) 2022 Attila Krasznahorkay.

// Project include(s).
#include "convert.hpp"

#include "TBranchConverterT.hpp"
#include "TBranchConverterVector.hpp"
#include "TFilePtr.hpp"
#include "cernlib_defs.hpp"
#include "error_handling.hpp"

// ROOT include(s).
#include <TClass.h>
#include <TFile.h>
#include <TKey.h>
#include <TObject.h>
#include <TString.h>
#include <TTree.h>

// CERNLIB include(s).
extern "C" {
#include <cfortran/cfortran.h>
#include <cfortran/hbook.h>
}

// System include(s).
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <set>
#include <vector>

namespace rth {

int convert(const std::string& ifilename, const std::string& ofilename) {

    // Open the input (ROOT) file.
    detail::TFilePtr ifile(TFile::Open(ifilename.c_str(), "READ"));
    if ((!ifile) || ifile->IsZombie()) {
        PRINT_ERROR("Could not open input file '" << ifilename << "'");
        return EXIT_FAILURE;
    }
    std::cout << "Opened input file '" << ifilename << "'" << std::endl;

    // Get the names of all objects/trees from the main directory of the file.
    TList* keyList = ifile->GetListOfKeys();
    if (!keyList) {
        PRINT_ERROR("Could not get the list of keys from file '" << ifilename
                                                                 << "'");
        return EXIT_FAILURE;
    }

    // The names of the trees that were already processed.
    std::set<std::string> processedTrees;
    // The tree(s) of the input file being processed.
    std::vector<TTree*> trees;
    // The container of "branch converters".
    detail::TBranchConverterVector converters;

    // Loop over all objects of the input ROOT file.
    for (Int_t i = 0; i < keyList->GetSize(); ++i) {

        // Access the TKey object.
        TKey* key = dynamic_cast<TKey*>(keyList->At(i));
        if (!key) {
            PRINT_ERROR("Could not access element " << i << " in the key list");
            return EXIT_FAILURE;
        }

        // Get the object.
        TObject* obj = ifile->Get(
            TString::Format("%s;%hi", key->GetName(), key->GetCycle()));
        if (!obj) {
            PRINT_ERROR("Couldn't access object with name '"
                        << key->GetName() << ";" << key->GetCycle() << "'");
            return EXIT_FAILURE;
        }

        // We're only interested in trees.
        if (obj->IsA()->InheritsFrom("TTree") == kFALSE) {
            std::cout << "*** Ignoring input object '" << key->GetName() << ";"
                      << key->GetCycle() << "' ***" << std::endl;
            continue;
        }

        // Only process a tree once. (In case it has multiple versions in the
        // input file.)
        if (processedTrees.insert(key->GetName()).second == false) {
            continue;
        }
        if (processedTrees.size() > 1) {
            std::cout << "*** More than one tree merged into a single HBOOK "
                      << "ntuple! ***" << std::endl;
        }

        // Access the input TTree object.
        TTree* tree = dynamic_cast<TTree*>(ifile->Get(key->GetName()));
        if (!tree) {
            PRINT_ERROR("There is a logic error in the code");
            return EXIT_FAILURE;
        }
        trees.push_back(tree);

        // Access all of the branches of the tree.
        TObjArray* branches = tree->GetListOfBranches();
        if (!branches) {
            PRINT_ERROR("There is a logic error in the code");
            return EXIT_FAILURE;
        }

        // Set up output variables for each (copyable) branch.
        for (Int_t j = 0; j < branches->GetEntriesFast(); ++j) {

            // Access the branch.
            TBranch* branch = dynamic_cast<TBranch*>(branches->At(j));
            if (!branch) {
                PRINT_ERROR("There is a logic error in the code");
                return EXIT_FAILURE;
            }

            // Get its type.
            TClass* cl = 0;
            EDataType dType = kOther_t;
            if (branch->GetExpectedType(cl, dType)) {
                PRINT_ERROR("Couldn't determine the type of branch '"
                            << branch->GetName() << "'");
                return EXIT_FAILURE;
            }

            // Ignore anything that's not a primitive type.
            if ((dType == kOther_t) || (dType == kNoType_t) ||
                (dType == kVoid_t)) {
                std::cout << "*** Skipping non-trivial branch '"
                          << branch->GetName() << "' ***" << std::endl;
                continue;
            }

/// Helper macro for the "converter" creation
#define MAKE_CONVERTER(TYPE)                                                \
    case k##TYPE:                                                           \
        converters->push_back(                                              \
            new detail::TBranchConverterT<TYPE>(*tree, branch->GetName())); \
        break

            // Create a "converter" for this branch.
            switch (dType) {
                MAKE_CONVERTER(Char_t);
                MAKE_CONVERTER(UChar_t);
                MAKE_CONVERTER(Short_t);
                MAKE_CONVERTER(UShort_t);
                MAKE_CONVERTER(Int_t);
                MAKE_CONVERTER(UInt_t);
                MAKE_CONVERTER(Long_t);
                MAKE_CONVERTER(ULong_t);
                MAKE_CONVERTER(Float_t);
                MAKE_CONVERTER(Double_t);
                default:
                    PRINT_ERROR("Unknown type found for branch '"
                                << branch->GetName() << "'");
                    return EXIT_FAILURE;
            }
        }
    }

/// Clean up
#undef MAKE_CONVERTER

    // Some security checks.
    if (trees.empty()) {
        PRINT_ERROR("No tree was found in '" << ifilename << "'");
        return EXIT_FAILURE;
    }
    if (trees.size() > 1) {
        const Long64_t entries = trees[0]->GetEntries();
        for (std::size_t i = 1; i < trees.size(); ++i) {
            if (trees[i]->GetEntries() != entries) {
                PRINT_ERROR(
                    "Not all input trees have the same number of "
                    "entries");
                return EXIT_FAILURE;
            }
        }
    }

    // Connect to all the identified branches.
    for (std::size_t i = 0; i < converters->size(); ++i) {
        CHECK_RETURN((*converters)[i]->connect());
    }

    // Create the HBOOK common block(s).
    HLIMIT(PAWC_SIZE);

    // Open the (potentially quite large) output HBOOK file.
    static const int HFILE_ID = 1;
    static const std::string acquisition = "acquisition";
    static const std::string n = "NQ";
    quest[9] = 65000;
    int record_size = 4095, istat = 0;
    HROPEN(HFILE_ID, const_cast<char*>(acquisition.data()),
           const_cast<char*>(ofilename.data()), const_cast<char*>(n.data()),
           record_size, istat);
    if (istat != 0) {
        PRINT_ERROR("Could not open output HBOOK file '" << ofilename << "'");
        return EXIT_FAILURE;
    }
    std::cout << "Opened output file '" << ofilename << "'" << std::endl;

    // Store the branch names in a format in memory that CERNLIB could
    // understand.
    static const std::size_t VARNAME_COUNT = 500;
    static const std::size_t VARNAME_LENGTH = 20;
    char varNames[VARNAME_COUNT][VARNAME_LENGTH];
    const std::size_t nVars = converters->size();
    if (nVars > VARNAME_COUNT) {
        PRINT_ERROR("Can not handle the large (" << nVars
                                                 << ") variable count");
        return EXIT_FAILURE;
    }
    for (std::size_t i = 0; i < nVars; ++i) {
        if (snprintf(varNames[i], VARNAME_LENGTH, "%s",
                     converters->at(i)->name().c_str()) > VARNAME_LENGTH) {
            PRINT_ERROR("Too long variable name ('" << converters->at(i)->name()
                                                    << "') found");
            return EXIT_FAILURE;
        }
    }

    // Create the row-wise ntuple.
    static const int NTUPLE_ID = 1;
    static const std::string data = "data";
    HBOOKN(NTUPLE_ID, const_cast<char*>(data.data()), nVars,
           const_cast<char*>(acquisition.data()), 5000, varNames);

    // Create the helper variable for filling the output ntuple.
    std::vector<float> variables(converters->size());

    // Copy every entry (in every input tree) into the output ntuple.
    const Long64_t entries = trees[0]->GetEntries();
    for (Long64_t entry = 0; entry < entries; ++entry) {

        // Load all variables into memory.
        for (std::size_t i = 0; i < trees.size(); ++i) {
            if (trees[i]->GetEntry(entry) < 1) {
                PRINT_ERROR("There was an error with loading event " << entry);
                return EXIT_FAILURE;
            }
        }

        // "Convert" them into the array that would be understood by CERNLIB.
        for (std::size_t i = 0; i < variables.size(); ++i) {
            variables[i] = (*converters)[i]->convert();
        }

        // Save the event into the output.
        HFN(NTUPLE_ID, variables.data());
    }

    // Close the output HBOOK file.
    static const std::string t = " ";
    int icycle = 0;
    HROUT(NTUPLE_ID, icycle, const_cast<char*>(t.data()));
    HREND(const_cast<char*>(acquisition.data()));
    std::cout << "Closed output file '" << ofilename << "'" << std::endl;

    // Return gracefully.
    return EXIT_SUCCESS;
}

}  // namespace rth
