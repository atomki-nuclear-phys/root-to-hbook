// Copyright (C) 2022 Attila Krasznahorkay.

// Project include(s).
#include "TBranchConverterVector.hpp"

namespace rth {
namespace detail {

TBranchConverterVector::TBranchConverterVector() : m_converters() {}

TBranchConverterVector::~TBranchConverterVector() {
    for (std::size_t i = 0; i < m_converters.size(); ++i) {
        TBranchConverterInterface* converter = m_converters[i];
        if (converter) {
            delete converter;
        }
    }
}

std::vector<TBranchConverterInterface*>* TBranchConverterVector::operator->() {

    return &m_converters;
}

const std::vector<TBranchConverterInterface*>*
TBranchConverterVector::operator->() const {

    return &m_converters;
}

std::vector<TBranchConverterInterface*>& TBranchConverterVector::operator*() {

    return m_converters;
}

const std::vector<TBranchConverterInterface*>&
TBranchConverterVector::operator*() const {

    return m_converters;
}

}  // namespace detail
}  // namespace rth
