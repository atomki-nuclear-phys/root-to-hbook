// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_HBOOK_FILENAME_HPP
#define ROOT_TO_HBOOK_HBOOK_FILENAME_HPP

// System include(s).
#include <string>

namespace rth {

/// Produce the name of a HBOOK file from a ROOT file's name
///
/// The function does something very simple. If it is given a file name ending
/// in '.root', it replaces that extension with '.hbook'. If the ending of the
/// filename is something else, it just attaches a '.hbook' extension to it.
///
/// @param fname The name of the (ROOT) file
/// @return The name of the (HBOOK) file to create
///
std::string hbook_filename(const std::string& fname);

}  // namespace rth

#endif  // ROOT_TO_HBOOK_HBOOK_FILENAME_HPP
