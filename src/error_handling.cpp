// Copyright (C) 2022 Attila Krasznahorkay.

// Local include(s).
#include "error_handling.hpp"

// System include(s).
#include <iomanip>
#include <iostream>
#include <sstream>

/// The length of path to strip from all file names
static const std::size_t STRIP_PATH_LENGTH = RTH_SOURCE_DIR_LENGTH;
/// The maximal path name length to print, and adjust all printouts to
static const std::size_t MAX_PATH_LENGTH = 40;

namespace rth {
namespace detail {

void print_error(const char* filename, int linenum,
                 const std::string& message) {

    // Create a "nice prefix" for the error message.
    std::ostringstream prefix;
    prefix << (filename + STRIP_PATH_LENGTH) << ":" << linenum;

    // Print the error message, correctly adjusted.
    std::cerr << std::setw(MAX_PATH_LENGTH) << std::left << prefix.str() << " "
              << message << std::endl;
    return;
}

}  // namespace detail
}  // namespace rth
