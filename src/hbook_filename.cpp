// Copyright (C) 2022 Attila Krasznahorkay.

// Project include(s).
#include "hbook_filename.hpp"

namespace rth {

std::string hbook_filename(const std::string& fname) {

    /// ROOT file name extension.
    static const std::string ROOT_EXTENSION = ".root";
    /// HBOOK file name extension.
    static const std::string HBOOK_EXTENSION = ".hbook";

    // Check if the file's name ends in '.root'.
    if (fname.rfind(ROOT_EXTENSION) == (fname.size() - ROOT_EXTENSION.size())) {
        std::string result = fname;
        result.erase(result.size() - ROOT_EXTENSION.size());
        result.append(HBOOK_EXTENSION);
        return result;
    }

    // If not, simply add the HBOOK extension.
    return (fname + HBOOK_EXTENSION);
}

}  // namespace rth
