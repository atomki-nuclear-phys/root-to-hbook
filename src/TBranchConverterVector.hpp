// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_TBRANCHCONVERTERVECTOR_HPP
#define ROOT_TO_HBOOK_TBRANCHCONVERTERVECTOR_HPP

// Project include(s).
#include "TBranchConverterInterface.hpp"

// System include(s).
#include <vector>

namespace rth {
namespace detail {

/// Helper type that owns a vector of branch converters
///
/// Much like @c TFilePtr, this type is only needed because we can't rely on
/// C++11 for writing (memory-)safe code. Instead I chose to write such a
/// container type by hand.
///
class TBranchConverterVector {

public:
    /// Default constructor
    TBranchConverterVector();
    /// Destructor
    ~TBranchConverterVector();

    /// Access the underlying vector using a pointer
    std::vector<TBranchConverterInterface*>* operator->();
    /// Access the underlying vector using a (const) pointer
    const std::vector<TBranchConverterInterface*>* operator->() const;

    /// Access the underlying vector using a reference
    std::vector<TBranchConverterInterface*>& operator*();
    /// Access the underlying vector using a (const) reference
    const std::vector<TBranchConverterInterface*>& operator*() const;

private:
    /// The vector of branch converters
    std::vector<TBranchConverterInterface*> m_converters;

};  // class TBranchConverterVector

}  // namespace detail
}  // namespace rth

#endif  // ROOT_TO_HBOOK_TBRANCHCONVERTERVECTOR_HPP
