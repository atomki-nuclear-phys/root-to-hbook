// Copyright (C) 2022 Attila Krasznahorkay.

// Project include(s).
#include "cmdl.hpp"
#include "convert.hpp"
#include "error_handling.hpp"
#include "hbook_filename.hpp"

/// Main function of the file converter executable
int main(int argc, char* argv[]) {

    // Read the command line arguments.
    const rth::cmdl_options options = rth::read_cmdl(argc, argv);

    // Process the input files.
    for (std::size_t i = 0; i < options.inputFileNames.size(); ++i) {
        CHECK_RETURN(
            rth::convert(options.inputFileNames[i],
                         rth::hbook_filename(options.inputFileNames[i])));
    }

    // Return gracefully.
    return EXIT_SUCCESS;
}
