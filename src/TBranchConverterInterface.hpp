// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_TBRANCHCONVERTERINTERFACE_HPP
#define ROOT_TO_HBOOK_TBRANCHCONVERTERINTERFACE_HPP

// System include(s).
#include <string>

namespace rth {
namespace detail {

/// Interface for all of the "branch converters"
///
/// Every primitive ROOT branch's payload needs to be converted into 32-bit
/// @c float values for CERNLIB's row-wise ntuple. Objects implementing this
/// interface take care of performing this conversion.
///
class TBranchConverterInterface {

public:
    /// Virtual destructor
    virtual ~TBranchConverterInterface() {}

    /// Connect to the input branch
    virtual int connect() = 0;
    /// Function converting the content of the branch into a @c float variable
    virtual float convert() const = 0;

    /// Get the name of the branch
    virtual const std::string& name() const = 0;

};  // class TBranchConverterInterface

}  // namespace detail
}  // namespace rth

#endif  // ROOT_TO_HBOOK_TBRANCHCONVERTERINTERFACE_HPP
