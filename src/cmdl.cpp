// Copyright (C) 2022 Attila Krasznahorkay.

// Local include(s).
#include "cmdl.hpp"

// System include(s).
extern "C" {
#include <getopt.h>
}
#include <cstdlib>
#include <iostream>

namespace rth {

/// Help message for the user.
static const char* HELP_MESSAGE =
    "\nroot-to-hbook <input file(s)>\n\n"
    "This executable can convert (simple) ROOT files/trees\n"
    "into HBOOK row-wise ntuples.\n\n"
    "The output file name(s) is/are automatically chosen.\n"
    "  - If the input file name ends in '.root', that is\n"
    "    replaced by '.hbook';\n"
    "  - Otherwise a '.hbook' extension is appended to the\n"
    "    file's name.\n";

cmdl_options read_cmdl(int argc, char* argv[]) {

    // Create the result object.
    cmdl_options result;

    // Parse the command line arguments.
    static const option options[] = {{"help", no_argument, 0, 0}};
    int c = 0, option_index = -1;
    while ((c = getopt_long(argc, argv, "h", options, &option_index)) != -1) {
        switch (c) {
            case 0:
                switch (option_index) {
                    case 0:
                        std::cout << HELP_MESSAGE << std::endl;
                        std::exit(EXIT_SUCCESS);
                    default:
                        std::cerr << HELP_MESSAGE << std::endl;
                        std::exit(EXIT_FAILURE);
                }
            case 'h':
                std::cout << HELP_MESSAGE << std::endl;
                std::exit(EXIT_SUCCESS);
            case '?':
            default:
                std::cerr << HELP_MESSAGE << std::endl;
                std::exit(EXIT_FAILURE);
        }
    }

    // Collect the input file name(s).
    for (int i = optind; i < argc; ++i) {
        result.inputFileNames.push_back(argv[i]);
    }

    // Return the configuration object.
    return result;
}

}  // namespace rth
