// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_ERROR_HANDLING_HPP
#define ROOT_TO_HBOOK_ERROR_HANDLING_HPP

// System include(s).
#include <cstdlib>
#include <sstream>
#include <string>

/// Macro for printing an error message
///
/// It's a very trivial macro, just providing a user-friendly interface to the
/// underlying @c rth::detail::print_error(...) function.
///
#define PRINT_ERROR(MSG)                                         \
    do {                                                         \
        std::ostringstream msg;                                  \
        msg << MSG;                                              \
        rth::detail::print_error(__FILE__, __LINE__, msg.str()); \
    } while (false)

/// Simple macro for checking return values in the code
///
/// The project uses @c int return types in all functions that may fail to
/// execute. (Including @c main(...) itself.) This macro is used in all such
/// calls to check the return values of the function calls.
///
#define CHECK_RETURN(EXP)                                       \
    do {                                                        \
        const int retval = EXP;                                 \
        if (retval != EXIT_SUCCESS) {                           \
            PRINT_ERROR("Failed to execute: '" << #EXP << "'"); \
            return retval;                                      \
        }                                                       \
    } while (false)

namespace rth {
namespace detail {

/// Function printing a formatted (error) message to the terminal
///
/// @param filename The name of the file that the message is coming from
///                 (Provided using @c __FILE__ with GCC and Clang.)
/// @param linenum  The line number that the message is coming from
///                 (Provided using @c __LINE__ with GCC and Clang.)
/// @param message  The (error) message to print
///
void print_error(const char* filename, int linenum, const std::string& message);

}  // namespace detail
}  // namespace rth

#endif  // ROOT_TO_HBOOK_ERROR_HANDLING_HPP
