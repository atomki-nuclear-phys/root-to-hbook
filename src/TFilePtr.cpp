// Copyright (C) 2022 Attila Krasznahorkay.

// Local include(s).
#include "TFilePtr.hpp"

// ROOT include(s).
#include <TFile.h>

// System include(s).
#include <iostream>

namespace rth {
namespace detail {

TFilePtr::TFilePtr(TFile* file) : m_file(file) {}

TFilePtr::~TFilePtr() {
    if (m_file) {
        const std::string fname = m_file->GetName();
        delete m_file;
        std::cout << "Closed input file '" << fname << "'" << std::endl;
    }
}

TFile* TFilePtr::operator->() {
    return m_file;
}

const TFile* TFilePtr::operator->() const {
    return m_file;
}

}  // namespace detail
}  // namespace rth
