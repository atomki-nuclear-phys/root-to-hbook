// Copyright (C) 2022 Attila Krasznahorkay.

// System include(s).
#include <string>
#include <vector>

namespace rth {

/// Command line options passed to the executable
struct cmdl_options {

    /// Input file name(s)
    std::vector<std::string> inputFileNames;

};  // struct cmdl_options

/// Read the command line options
///
/// @param argc The count of command line arguments (from @c main())
/// @param argv The command line arguments (from @c main())
/// @return A simple struct with all of the command line arguments
///
cmdl_options read_cmdl(int argc, char* argv[]);

}  // namespace rth
