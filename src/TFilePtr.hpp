// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_TFILEPTR_HPP
#define ROOT_TO_HBOOK_TFILEPTR_HPP

// Forward declaration(s).
class TFile;

namespace rth {
namespace detail {

/// Simple smart pointer around a @c TFile object
///
/// Since this code needs to work with any C++ version starting from C++98,
/// I can't rely on @c std::unique_ptr. Instead I just use a specific type for
/// @c TFile.
///
class TFilePtr {

public:
    /// Constructor, with a @c TFile pointer
    TFilePtr(TFile* file);
    /// Destructor, deleting the managed object
    ~TFilePtr();

    /// Access the managed pointer
    TFile* operator->();
    /// Access the managed (const) pointer
    const TFile* operator->() const;

    /// Operator checking whether the pointer is valid
    operator bool() const { return m_file; }

private:
    /// The managed @c TFile pointer
    TFile* m_file;

};  // class TFilePtr

}  // namespace detail
}  // namespace rth

#endif  // ROOT_TO_HBOOK_TFILEPTR_HPP
