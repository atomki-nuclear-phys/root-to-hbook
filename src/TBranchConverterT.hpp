// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_TBRANCHCONVERTERT_HPP
#define ROOT_TO_HBOOK_TBRANCHCONVERTERT_HPP

// Project include(s).
#include "TBranchConverterInterface.hpp"

// ROOT include(s).
#include <TTree.h>

// System include(s).
#include <cstdlib>
#include <string>

namespace rth {
namespace detail {

/// "Branch converter" for a specific primitive type
///
/// Every primitive ROOT branch can be handled in the same way, we only need to
/// specify the concrete type that we want to handle, once.
///
template <typename TYPE>
class TBranchConverterT : public TBranchConverterInterface {

public:
    /// Constructor
    TBranchConverterT(TTree& tree, const std::string& name)
        : m_tree(tree), m_name(name), m_value(0) {}

    /// @name Function(s) re-implemented from @c TBranchConverterInterface
    /// @{

    /// Connect to the input branch
    virtual int connect() {
        if (m_tree.SetBranchAddress(m_name.c_str(), &m_value) < 0) {
            return EXIT_FAILURE;
        } else {
            return EXIT_SUCCESS;
        }
    }

    /// Function converting the content of the branch into a @c float variable
    virtual float convert() const { return static_cast<float>(m_value); }

    /// Get the name of the branch
    virtual const std::string& name() const { return m_name; }

    /// @}

private:
    /// The input tree
    TTree& m_tree;
    /// The name of the branch
    std::string m_name;
    /// The primitive variable
    TYPE m_value;

};  // class TBranchConverterT

}  // namespace detail
}  // namespace rth

#endif  // ROOT_TO_HBOOK_TBRANCHCONVERTERT_HPP
