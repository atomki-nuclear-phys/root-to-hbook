// Copyright (C) 2022 Attila Krasznahorkay.
#ifndef ROOT_TO_HBOOK_CONVERT_HPP
#define ROOT_TO_HBOOK_CONVERT_HPP

// System include(s).
#include <string>

namespace rth {

/// Function converting all trees of a ROOT file into HBOOK RW ntuples
///
/// This is the function called from the @c main(...) function for every input
/// file specified on the command line. It looks for all trees in (the main
/// directory of) the input file, and produces a row-wise ntuple in an output
/// hbook file with their content.
///
/// @param ifile The name of the input ROOT file
/// @param ofile The name of the output HBOOK file
/// @return @c 0 if the function ran successfully, some other value otherwise
///
int convert(const std::string& ifile, const std::string& ofile);

}  // namespace rth

#endif  // ROOT_TO_HBOOK_CONVERT_HPP
