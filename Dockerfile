# Copyright (C) 2022 Attila Krasznahorkay.

# Build on top of the "CDA image".
FROM krasznaa/cda-build:ubuntu-12.04

# Copy the sources into the image for its build.
COPY CMakeLists.txt /root/source/
COPY src/* /root/source/src/
COPY cmake/* /root/source/cmake/

# Build and install the code.
RUN mkdir -p /root/build                                                 && \
    cd /root/build/                                                      && \
    cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=FALSE /root/source/ && \
    make                                                                 && \
    make install                                                         && \
    rm -rf /root/build /root/source
