#!/bin/bash
#
# Copyright (C) 2022 Attila Krasznahorkay.
#
# Helper script for running PAW using the Docker image built with the
# configuration stored in this directory.
#

# Transmit errors.
set -e
set -o pipefail

# Start the container, with a whole batch of options.
docker run -it --rm -v "${PWD}":"${PWD}" -w "${PWD}"                       \
   --user "$(id -u):$(id -g)"                                              \
   -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=${DISPLAY} -h "${HOSTNAME}" \
   krasznaa/paw-executor:ubuntu-12.04 paw "$@"
