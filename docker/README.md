# PAW(++) Image / Helper Code

This directory contains 2 things:
  - The build configuration for a Docker image that can be used to build this
    project, and to also run PAW(++).
  - Helper scripts for running PAW(++) using that Docker container.

## Building the Docker Image

The Docker image can be built as easily as:

```
docker build -t krasznaa/paw-executor:ubuntu-12.04 ./docker/ubuntu-12.04/
```

## Running the Docker Container

Once the image is built and uploaded to DockerHub, the scripts in this directory
can be used to conveniently start PAW and PAW++.
