// Copyright (C) 2022 Attila Krasznahorkay.

// System include(s).
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

/// Macro testing that an expression would be true
#define EXPECT_TRUE(EXP)                                        \
    do {                                                        \
        const bool result = EXP;                                \
        if (result == false) {                                  \
            std::cerr << __FILE__ << ":" << __LINE__            \
                      << " Failed test: " << #EXP << std::endl; \
            return EXIT_FAILURE;                                \
        }                                                       \
    } while (false)

/// Helper template for printing a value
template <typename T>
std::string printedValue(const T& value) {
    std::ostringstream str;
    str << value;
    return str.str();
}

/// Macro testing that two values would be the same
#define EXPECT_EQ(EXP1, EXP2)                                                 \
    do {                                                                      \
        const bool result = (EXP1 == EXP2);                                   \
        if (result == false) {                                                \
            std::cerr << __FILE__ << ":" << __LINE__ << " " << #EXP1 << " ('" \
                      << printedValue(EXP1) << "') != " << #EXP2 << " ('"     \
                      << printedValue(EXP2) << "')" << std::endl;             \
            return EXIT_FAILURE;                                              \
        }                                                                     \
    } while (false)
