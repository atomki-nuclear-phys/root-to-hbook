// Copyright (C) 2022 Attila Krasznahorkay.
//
// Unit test(s) for the hbook_filename(...) function.
//

// Project include(s).
#include "../src/hbook_filename.hpp"

#include "test_helpers.hpp"

// System include(s).
#include <cstdlib>

int main() {

    // Perform some simple test(s).
    EXPECT_EQ(rth::hbook_filename("/foo/bar.root"), "/foo/bar.hbook");
    EXPECT_EQ(rth::hbook_filename("/bar/foo.bar"), "/bar/foo.bar.hbook");

    // Return gracefully.
    return EXIT_SUCCESS;
}
